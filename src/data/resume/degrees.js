const degrees = [
  {
    school: 'Tulsa Community College',
    degree: 'A.S Computer Information Systems',
    link: 'https://www.tulsacc.edu/',
    year: 2023,
  },
  {
    school: 'Hack Reactor',
    degree: 'JavaScript and Python Immersive Bootcamp',
    link: 'https://www.hackreactor.com/',
    year: 2022,
  },
  {
    school: 'Siena Heights University',
    degree: 'B.A.Sc Surgical Technology',
    link: 'https://www.sienaheights.edu/',
    year: 2017,
  },
  {
    school: 'Community College of the Air Force',
    degree: 'A.A.Sc Surgical Technology',
    link: 'https://www.airuniversity.af.edu/Barnes/CCAF/',
    year: 2015,
  },
];

export default degrees;
